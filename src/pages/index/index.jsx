import { Component } from "react";
import { View, Text, Button, Input, Switch } from "@tarojs/components";
import "./index.scss";

export default class Index extends Component {
  componentWillMount() {}

  componentDidMount() {
    console.log(process.env.TARO_ENV);
  }

  render() {
    return (
      <View className="index">
        <View>我是View</View>
        <Text>我是Text</Text>
        <Input placeholder="将会获取焦点" />
        <Button>测试按钮</Button>
        <Switch checked />
      </View>
    );
  }
}
